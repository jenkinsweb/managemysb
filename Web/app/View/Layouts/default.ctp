<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title><?php echo $title_for_layout; ?> | ManageMySB</title>

	<?php
		echo $this->Html->meta("icon");
		echo $this->fetch("meta");

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap.responsive.min');
		echo $this->Html->css('main');
		echo $this->fetch("css");

	?>
</head>
<body>
	<div id="wrapper">
		<header>
			<nav id="mainNav">
			</nav>
			<div class='flash'>
				<?php echo $this->Session->flash(); ?>
			</div>
		</header>
		<div id="content">
			<?php echo $this->fetch("content"); ?>
		</div>
		<footer></footer>

		<?php
			echo $this->Html->script("jquery");
			echo $this->Html->script("bootstrap.min");
			echo $this->fetch("script");
		?>
	</div>
</body>
</html>