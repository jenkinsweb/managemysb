<?php
	
	class Customer extends AppModel
	{
		public $belongsTo = "Company";
		public $hasMany = array(
				"Project" = array("className" => "Project"),
				"Invoice" = array("className" => "Invoice"),
				"InvoiceItem" = array("className" => "InvoiceItem"),
				"Contact" = array("className" => "Contact")
			);
	}

?>