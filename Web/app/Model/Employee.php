<?php

	class Employee extends AppModel
	{
		public $belongsTo = "Company";
		public $hasMany = array (
				"TimeRecord" => array("className" => "TimeRecord");
			);
	}

?>