<?php

	class TimeRecord extends AppModel
	{
		public belongsTo = array("Employee", "Project");
		public hasAndBelongsToMany = array(
				"Invoice" => array("className" => "Invoice")
			);
	}

?>