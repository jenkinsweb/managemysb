<?php

	class Project extends AppModel
	{
		public $belongsTo = "Customer";
		public $hasMany = array(
				"TimeRecord" = array("className" => "TimeRecord")
			);
	}

?>